En este directorio encontrá muchos ejemplos de script para bash.

# NOTAS #

* Variables:
	**Correcto:
		```
		variable=valor
		```
		ejemplo:
		```
		nombre="Johnny"
		```
	**incorrecto:
		```
		nombre = "johnny"
		```
		Es incorrecto porque en éste caso existe espacios en la instrucción

* Condicional if

```
read -p "Continuar y/n: " -r option
if [ "$option" == "y" ]
then
	echo "continua"
else
	echo "se detiene"
if

```
Aquí se comparan dos string, tener especial atención en los espacios del if, sino se tienen en cuenta no reconocerá la instrucción.
Note que para retornar el valor de la variable **option** se ha dejado dentro de las comillas dobles (" ").
