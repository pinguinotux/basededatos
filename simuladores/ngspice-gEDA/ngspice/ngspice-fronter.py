#! /usr/bin/python
# -*- coding: utf-8 -*-

from Tkinter import *
import os
import subprocess

#Crear circuito
def crearCircuito():
	os.system("gnetlist -o circuito.cir -g spice-sdb *.sch")

# Crear ventana y frame
root= Tk()
frameRight = Frame(root)
frameRight.pack(side = RIGHT)
root.geometry("640x480")

# Crear boton crearcircuito
botonCrearCircuito = Button(frameRight, text="Crear Circuito", command=crearCircuito)
botonCrearCircuito.pack(side = RIGHT)

# Lanzar ventana principal
root.mainloop()
