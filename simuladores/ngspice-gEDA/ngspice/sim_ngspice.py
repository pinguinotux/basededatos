#! /usr/bin/python


import string 
import os
import subprocess


#Constantes y Variables
comparador = "*==============  Begin SPICE netlist of main design ============\n"


def longitudArchivo(fichero):
    f = open(fichero, 'r')
    l = len(f.readlines())
    f.close()
    return l


def list_Sim():
    os.system('echo `ls -1 *.sim` > /tmp/archivo.sim')
    archivo_sim = open('/tmp/archivo.sim', 'r')
    lineas_sim = str(archivo_sim.readline())
    if	lineas_sim == "\n" :
	print "No hay simulaciones para cargar"
	archivo_sim.close()
	return 0
    else:
	print "Simulaciones cargadas: \n" + lineas_sim
	archivo_sim.close()
	lineas_sim = lineas_sim.rstrip('\n')
	return lineas_sim.split(' ') 


# Encontrar circuito 
def find_Cir():
    os.system('echo `ls *.cir` > /tmp/archivo.cir')
    archivo_cir = open('/tmp/archivo.cir', 'r')
    linea_cir = str(archivo_cir.readline())
    archivo_cir.close()
    linea_cir = linea_cir.rstrip('\n')
    print "circuito a simular: " + linea_cir
    return linea_cir


# Leer el circuito existente en el directorio
def read_Cir():
    circuit = find_Cir()
    archivo = open(circuit, 'r')
    linea   = str(archivo.readline())
    lista = [circuit]
    while linea != "":
	linea = archivo.readline()
	if linea == comparador:
	    while linea != "":
		linea = str(archivo.readline())
		lista.append(linea)
		print linea.rstrip('\n')
    archivo.close()
    return lista

# Nueva simulacion
def new_sim():
    new_name_sim = raw_input("Introducir nombre sin extension: ")+".sim"
    lista = [".control\n", "simulacion\n", "plot\n", "hardcopy\n", ".endc\n"]
	#f = open(new_name_sim + ".sim", 'w')
    #f.write(new_name_sim)
    #f.close()
    return [libro.write_book(new_name_sim, lista), new_name_sim]

# Cualquier lista sera guardada en un
# archivo de nombre name_file
def save_file(name_file, lista):
    f = open(name_file, 'w')
    for i in lista:
	f.write(i)
    f.close()

class libros:
    def __init__(self):
	pass
    def write_book(self, nombre, lista):
	self.f = open(nombre, 'w')
	for i in lista:
	    self.f.write(i)
	self.f.close()
	return "libro "+nombre+" escrito"
    def read_book(self, nombre):
	self.lista = []
	self.f = open(nombre, 'r')
	self.linea = self.f.readline()
	print nombre
	while self.linea != '\n' and self.linea != "":
	    self.lista.append(self.linea)
	    self.linea = self.f.readline()     
	    print self.linea
	self.f.close()
	print self.lista
	return self.lista


def load_sim(name_Sim):
    print name_Sim
    file_sim = libro.read_book(name_Sim)
    while(True):
	print "\n#########################" 
	print name_Sim 
	for i in file_sim:
	    print i.rstrip('\n')
	print "########################"
	for i in branch:
	    print i.rstrip('\n')
	print "#########################" 
	print "#########################" 
	print "###       MENU        ###"
	print "#########################"
	print "# s ->  tipo simulacion #"
	print "# p ->  plot variable   #"
	print "# h ->  hcopy variable  #"
	print "# simular? y/n          #"
	print "#########################"
	option = raw_input("Seleccione la opcion: ")
	if option == 's':
	    print "Tipo de Simulacion"
	    print " t -> transitoria"
	    print " d -> dc"
	    option = raw_input("Selecione la opcion: ")
	    if option == 't':
		file_sim[1] = "tran"
		inicio = raw_input("pasos: ")
		final = raw_input("Tiempo de parada: ")
		pasos = raw_input("Tiempo de inicio: ")
		file_sim[1] = file_sim[1]+" "+inicio+" "+final+" "+pasos+"\n"
	    elif option == 'd':
		file_sim[1] = "dc"
		fuente = raw_input("primera fuente dc: ")
		vmin = raw_input("tension minima: ")
		vmax = raw_input("tension maxima: ")
		pasos = raw_input("pasos fuente: ")
		option = raw_input("otra fuente? y/n: ")
		if option == 'y':
		    fuente1 = raw_input("segunda fuente dc: ")
		    vmin1 = raw_input("tension minima: ")
		    vmax1 = raw_input("tension maxima: ")
		    pasos1 = raw_input("pasos fuente: ")
		    file_sim[1]=file_sim[1] +" "+fuente+" "+vmin+" "+vmax+" "+pasos+" "+fuente1+" "+vmin1+" "+vmax1+" "+pasos1+"\n"
		elif option == 'n':
		    file_sim[1] = file_sim[1] +" "+fuente+" "+vmin+" "+vmax+" "+pasos+"\n"
		else:
		    print "opcion incorrecta"
	    else:
		print "opcion incorrecta"
	elif option == 'p':
	    file_sim[2] = "plot"+" "+raw_input("Ingrese la variable: ")+"\n"
	elif option == 'h':
	    file_sim[3] = "hardcopy"+" "+name_Sim.rstrip('.sim')+".ps"+" "+raw_input("Ingrese la variable: ")+"\n"
	elif option == 'y':
	    break
	else:
	    print "Opcion Incorrecta"	
    f=open(name_Sim,'w')
    for i in file_sim:
	f.write(str(i))
    f.close()
    new_file=open('sim_start.sh', 'w')
    circuito = find_Cir()
    circuito = circuito.rstrip('.cir')
    entrada = "ngspice -i "+circuito+".cir "+str(circuito)+".op "+str(name_Sim)
    print entrada  
    new_file.write(entrada)
    new_file.close()
    os.system("chmod +x sim_start.sh")
    os.system("./sim_start.sh")


def sim():
    print "\n#########################" 
    print "###       MENU        ###"
    print "#########################"
    print "# n -> new simulation   #"
    print "# l -> load simulation  #"
    print "#########################"
    option = raw_input("Introducir orden: ")
    if option == 'n':
	print "new simulation"
	list_new_sim = new_sim()
	print list_new_sim[0]+" "+list_new_sim[1]
	return list_new_sim[1]
    elif option == 'l':
	load_list = list_Sim()
	if load_list[0] == 0:
	    print "no hay simulaciones que cargar"
	    option = raw_input("Desea crear una simualcion? y/n")
	    if option == 'y':
		return new_sim()
	    else:
		print "reiniciar"
	else:
	    x=0
	    for i in load_list:
		x=x+1
		print x," -> ", i
	    option = int(raw_input("Escoja la simulacion a cargar: "))-1
	    return load_list[option]
    else:
	print "Opcion no contemplada en el programa"
    
    
def main():
    global libro
    libro = libros()
    global branch
    branch = read_Cir()  
    list_Sim()
    load_sim(sim())


#if __name__ == '__sim_ngspice.py__':
main()


#line.rstrip('\n') -> quita el salto de linea de una linea
#str(archivo.readlines()) -> lee una linea de un archivo
#os.system() -> ejecuta comandos por fuera de python
# cadena.split(' ') separa la cadena en una lista
#BUG: POsible en los archivos que terminan en r.cir queda pendiente
# de revision
