# Guardar Nuevos símbolos #

Los símbolos *gEDA* deben ser guardados en un directorio creado por usted
con una referencia para que los encuentre *gschem* de la siguiente manera:

* crear el archivo **gafrc** con el siguiente contenido

    * ;dirección a carpeta con símbolos propios para gschem 
    * (component-library "path-directorio/directorio")

* mover el archivo **gafrc** al directorio oculto de gEDA. 

## Ejemplo: ##

```
$ touch gafrc
$ echo "(component-library "'"'"/home/user/basededatos/symbolGEDA"'"'")" >> gafrc
$ mv gatrc ~/.gEDA
```
**Nota**: user hace referencia al nombre de su usuario. No acepta
path relativo (ejemplo ~/... O ../).

## Script: ##

Puede hacer uso del script **simbolo.sh** que está contenido en éste
directorio, ejecutelo de la siguiente manera:

```
$ ./simbolo.sh
```

Fuente: http://charliexray.blogspot.com.co/2011/01/subcircuitos-en-gschem.html
