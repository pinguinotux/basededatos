#! /bin/bash

path1="/home/`whoami`/basededatos/symbolGEDA"
path2="/home/`whoami`/basededatos/symbol_gEDA_ngspice/voltage_current_source"
path3="/home/`whoami`/basededatos/symbol_gEDA_ngspice/switches"
path4="/home/`whoami`/basededatos/symbol_gEDA_ngspice/ngspice"
path5="/home/`whoami`/basededatos/symbol_gEDA_ngspice/transistors"
path6="/home/`whoami`/basededatos/symbol_gEDA_ngspice/ammeter"
echo "Creando archivo gafrc en .gEDA, el cual permite el enlace 
    de simbolos creados por usted para los esquematicos hechos
    con schem"

echo ";path para simbolos creados por usted" > gafrc
echo "(component-library "'"'$path1'"'")" >> gafrc
echo "(component-library "'"'$path2'"'")" >> gafrc
echo "(component-library "'"'$path3'"'")" >> gafrc
echo "(component-library "'"'$path4'"'")" >> gafrc
echo "(component-library "'"'$path5'"'")" >> gafrc
echo "(component-library "'"'$path6'"'")" >> gafrc

mv -i -v gafrc ~/.gEDA

echo "Operacion realizada"
