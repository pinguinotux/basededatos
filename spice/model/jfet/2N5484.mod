**********
*http://www.datasheetarchive.com/files/spicemodels/misc/modelos/spice_complete/jfet.lib
*SRC=2N5484;JN5484;JFETs N;VHF/UHF;25V 10mA, Low Noise
.MODEL JN5484 NJF (VTO=-2.2 BETA=3.84M LAMBDA=8.85M RD=21
+ RS=18.9 IS=6.77F PB=1 FC=.5 CGS=6P CGD=2.8P AF=1 KF=2.488E-17)
* Siliconix 35 Volt .03 Amp 150 ohm Dep-Mode N-Channel 10-09-1990
* High Performance Amp 05-14-1991
