*********************************************************
* Model switch											*
* Name	Parameter			Unit	Default	Switch_model*
* VT	threshold voltage	V		0.0		SW			*
* IT	threshold current	A		0.0		CSW			*
* VH	hysteresis voltage	V		0.0		SW			*
* IH	hysteresis current	A		0.0		CSW			*
* RON	on resistance		Ω		1.0		SW, CSW		*
* ROFF	off resistance		Ω		1.0e+12 SW, CSW		*
*********************************************************
.MODEL swmodel	sw vt=1.0 ron=1 roff=1.0e+12 
