" Vim color file - ashen
" Generated by http://bytefluent.com/vivify 2015-09-29
set background=light
if version > 580
	hi clear
	if exists("syntax_on")
		syntax reset
	endif
endif

set t_Co=256
let g:colors_name = "ashen"

"hi IncSearch -- no settings --
"hi WildMenu -- no settings --
"hi SignColumn -- no settings --
"hi Folded -- no settings --
"hi TabLineSel -- no settings --
"hi StatusLineNC -- no settings --
"hi CTagsMember -- no settings --
"hi CTagsGlobalConstant -- no settings --
"hi DiffText -- no settings --
"hi ErrorMsg -- no settings --
"hi Ignore -- no settings --
"hi Todo -- no settings --
"hi StatusLine -- no settings --
hi Normal guifg=#000000 guibg=#e0e0e0 guisp=#e0e0e0 gui=NONE ctermfg=NONE ctermbg=254 cterm=NONE
"hi CTagsImport -- no settings --
"hi Search -- no settings --
"hi CTagsGlobalVariable -- no settings --
"hi SpellRare -- no settings --
"hi EnumerationValue -- no settings --
"hi CursorLine -- no settings --
"hi Union -- no settings --
"hi TabLineFill -- no settings --
"hi Question -- no settings --
"hi WarningMsg -- no settings --
"hi VisualNOS -- no settings --
"hi DiffDelete -- no settings --
"hi ModeMsg -- no settings --
"hi CursorColumn -- no settings --
"hi FoldColumn -- no settings --
"hi EnumerationName -- no settings --
"hi MoreMsg -- no settings --
"hi SpellCap -- no settings --
"hi VertSplit -- no settings --
"hi DiffChange -- no settings --
"hi Cursor -- no settings --
"hi SpellLocal -- no settings --
"hi Error -- no settings --
"hi SpecialKey -- no settings --
"hi DefinedName -- no settings --
"hi MatchParen -- no settings --
"hi LocalVariable -- no settings --
"hi SpellBad -- no settings --
"hi CTagsClass -- no settings --
"hi Underlined -- no settings --
"hi DiffAdd -- no settings --
"hi TabLine -- no settings --
"hi clear -- no settings --
hi SpecialComment guifg=#32329e guibg=NONE guisp=NONE gui=NONE ctermfg=61 ctermbg=NONE cterm=NONE
hi Typedef guifg=#dcdce0 guibg=NONE guisp=NONE gui=NONE ctermfg=254 ctermbg=NONE cterm=NONE
hi Title guifg=#000000 guibg=#ffffcc guisp=#ffffcc gui=NONE ctermfg=NONE ctermbg=230 cterm=NONE
hi PreCondit guifg=#000080 guibg=NONE guisp=NONE gui=bold ctermfg=18 ctermbg=NONE cterm=bold
hi Include guifg=#000080 guibg=NONE guisp=NONE gui=bold ctermfg=18 ctermbg=NONE cterm=bold
hi NonText guifg=#6f8b17 guibg=NONE guisp=NONE gui=NONE ctermfg=64 ctermbg=NONE cterm=NONE
hi Debug guifg=#32329e guibg=NONE guisp=NONE gui=NONE ctermfg=61 ctermbg=NONE cterm=NONE
hi PMenuSbar guifg=NONE guibg=#84986C guisp=#84986C gui=NONE ctermfg=NONE ctermbg=101 cterm=NONE
hi Identifier guifg=#000000 guibg=NONE guisp=NONE gui=NONE ctermfg=NONE ctermbg=NONE cterm=NONE
hi SpecialChar guifg=#32329e guibg=NONE guisp=NONE gui=NONE ctermfg=61 ctermbg=NONE cterm=NONE
hi Conditional guifg=#4848f3 guibg=NONE guisp=NONE gui=bold ctermfg=63 ctermbg=NONE cterm=bold
hi StorageClass guifg=#4848f3 guibg=NONE guisp=NONE gui=bold ctermfg=63 ctermbg=NONE cterm=bold
hi Special guifg=#32329e guibg=NONE guisp=NONE gui=NONE ctermfg=61 ctermbg=NONE cterm=NONE
hi LineNr guifg=#e0e0e0 guibg=#84986C guisp=#84986C gui=NONE ctermfg=254 ctermbg=101 cterm=NONE
hi Label guifg=#4848f3 guibg=NONE guisp=NONE gui=bold ctermfg=63 ctermbg=NONE cterm=bold
hi PMenuSel guifg=#88dd88 guibg=#949698 guisp=#949698 gui=NONE ctermfg=114 ctermbg=246 cterm=NONE
hi Delimiter guifg=#32329e guibg=NONE guisp=NONE gui=NONE ctermfg=61 ctermbg=NONE cterm=NONE
hi Statement guifg=#006600 guibg=NONE guisp=NONE gui=bold ctermfg=22 ctermbg=NONE cterm=bold
hi Comment guifg=#c03417 guibg=NONE guisp=NONE gui=italic ctermfg=1 ctermbg=NONE cterm=NONE
hi Character guifg=#9d7d4a guibg=NONE guisp=NONE gui=NONE ctermfg=137 ctermbg=NONE cterm=NONE
hi Float guifg=#9d7d4a guibg=NONE guisp=NONE gui=NONE ctermfg=137 ctermbg=NONE cterm=NONE
hi Number guifg=#9d7d4a guibg=NONE guisp=NONE gui=NONE ctermfg=137 ctermbg=NONE cterm=NONE
hi Boolean guifg=#669900 guibg=NONE guisp=NONE gui=NONE ctermfg=64 ctermbg=NONE cterm=NONE
hi Operator guifg=#01c1c7 guibg=NONE guisp=NONE gui=NONE ctermfg=44 ctermbg=NONE cterm=NONE
hi Define guifg=#000080 guibg=NONE guisp=NONE gui=bold ctermfg=18 ctermbg=NONE cterm=bold
hi Function guifg=#660066 guibg=NONE guisp=NONE gui=NONE ctermfg=53 ctermbg=NONE cterm=NONE
hi PreProc guifg=#000080 guibg=NONE guisp=NONE gui=bold ctermfg=18 ctermbg=NONE cterm=bold
hi Visual guifg=NONE guibg=#00ccff guisp=#00ccff gui=NONE ctermfg=NONE ctermbg=45 cterm=NONE
hi Exception guifg=#4848f3 guibg=NONE guisp=NONE gui=bold ctermfg=63 ctermbg=NONE cterm=bold
hi Keyword guifg=#c03417 guibg=NONE guisp=NONE gui=NONE ctermfg=1 ctermbg=NONE cterm=NONE
hi Type guifg=#4848f3 guibg=NONE guisp=NONE gui=bold ctermfg=63 ctermbg=NONE cterm=bold
hi PMenu guifg=#dddddd guibg=#545658 guisp=#545658 gui=NONE ctermfg=253 ctermbg=240 cterm=NONE
hi Constant guifg=#9d7d4a guibg=NONE guisp=NONE gui=NONE ctermfg=137 ctermbg=NONE cterm=NONE
hi Tag guifg=#32329e guibg=NONE guisp=NONE gui=NONE ctermfg=61 ctermbg=NONE cterm=NONE
hi String guifg=#eb2c2c guibg=NONE guisp=NONE gui=NONE ctermfg=13 ctermbg=NONE cterm=NONE
hi PMenuThumb guifg=NONE guibg=#e0e0e0 guisp=#e0e0e0 gui=NONE ctermfg=NONE ctermbg=254 cterm=NONE
hi Repeat guifg=#4848f3 guibg=NONE guisp=NONE gui=bold ctermfg=63 ctermbg=NONE cterm=bold
hi Directory guifg=#8b8b8b guibg=NONE guisp=NONE gui=NONE ctermfg=245 ctermbg=NONE cterm=NONE
hi Structure guifg=#4848f3 guibg=NONE guisp=NONE gui=bold ctermfg=63 ctermbg=NONE cterm=bold
hi Macro guifg=#000080 guibg=NONE guisp=NONE gui=bold ctermfg=18 ctermbg=NONE cterm=bold
hi cursorim guifg=#ffffff guibg=#96cdcd guisp=#96cdcd gui=bold ctermfg=15 ctermbg=152 cterm=bold
hi underline guifg=#afafff guibg=NONE guisp=NONE gui=NONE ctermfg=147 ctermbg=NONE cterm=NONE
hi pythonbuiltin guifg=#657b83 guibg=NONE guisp=NONE gui=NONE ctermfg=66 ctermbg=NONE cterm=NONE
hi phpstringdouble guifg=#e2e4e5 guibg=NONE guisp=NONE gui=NONE ctermfg=254 ctermbg=NONE cterm=NONE
hi htmltagname guifg=#e2e4e5 guibg=NONE guisp=NONE gui=NONE ctermfg=254 ctermbg=NONE cterm=NONE
hi javascriptstrings guifg=#e2e4e5 guibg=NONE guisp=NONE gui=NONE ctermfg=254 ctermbg=NONE cterm=NONE
hi htmlstring guifg=#e2e4e5 guibg=NONE guisp=NONE gui=NONE ctermfg=254 ctermbg=NONE cterm=NONE
hi phpstringsingle guifg=#e2e4e5 guibg=NONE guisp=NONE gui=NONE ctermfg=254 ctermbg=NONE cterm=NONE
hi lcursor guifg=NONE guibg=#000000 guisp=#000000 gui=NONE ctermfg=NONE ctermbg=NONE cterm=NONE
