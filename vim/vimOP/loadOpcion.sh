#!/bin/bash

echo "Hola `whoami`, este script borrara su configuracion de vim local y agregara una nueva"

read -p "Desea continuar? y/n: " -r opcion

echo $opcion

if [ "$opcion" == "y" ]
then (rm -rfv ~/.vim; ln -sv `pwd`/vim ~/.vim)
else
	echo "No se instalará nada"
fi
