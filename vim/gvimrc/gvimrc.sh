#! /bin/bash

echo "Hola `whoami`,"
echo "Este comando creará un enlace simbólico del archivo .gvimrc
del usuario `pwd`"

echo "Escoja uno de los .gvimrc a usar" 

ls -d */

read -p "digite cual: " -r gvimrc

rm -v ~/.gvimrc
ln -sv `pwd`/$gvimrc/gvimrc ~/.gvimrc

echo "finalizado"
echo "Esperamos que sea de su agrado `whoami`"
echo "Att: Johnny Cubides"
echo "grupo pinguino Tux"
