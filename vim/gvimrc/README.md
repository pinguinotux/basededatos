# Archivo de configuración para Vim #

El archivo de configuración **.gvimrc** se usa para facilitar la escritura
en el momento de editar archivos particulares.

**Nota:** el uso del sh hará un enlace simbólico, no una copia.

En este directorio encontrará configuraciones en diferentes directorios.
En el caso particular solo hay dos : jgcc y crpg, pero agregar más es 
cuestión de hacerlo de forma manual.

Para hacer uso de alguna configuración basta con ejecutar el script *gvimrc.sh*
el cual le pedirá ingresar el nombre de alguna de las configuraciones posibles.

**nota**: el nombre a ingresar debe hacerse sin el slash (/).

**Ejemplo**:

```
Hola johnny,
Este comando creará un enlace simbólico del archivo .gvimrc
del usuario /home/johnny/basededatos/vim/vimrc
Escoja uno de los .gvimrc a usar
jgcc/
$ digite cual: jgcc
removed ‘/home/johnny/.vimrc’
‘/home/johnny/.gvimrc’ -> ‘/home/johnny/basededatos/vim/gvimrc/jgcc/gvimrc’
finalizado
Esperamos que sea de su agrado johnny
Att: Johnny Cubides
grupo pinguino Tux
```

**Nota:** En el directorio jgcc hay un README que le permitirá ver algunas funciones
implementadas en el archivo *.gvimrc*
