# colorscheme #

En éste directorio encontrará temas que le pueden ser de su agrado

Para instalar los temas haga uso del script **themesVim.sh**

Ejemplo de uso:

```
$ ./themesVim.sh
```

**nota**: Tenga en cuenta que si ya había creado un directorio con ese nombre
debe eliminarlo y pasar su contenido al directorio al cual apunta el 
enlace simbólico.

Hacer algo como esto:
```
$ cp -i ~/.vim/colors/* ~/basededatos/vim/colorscheme/colors/
$ rm -rf ~/.vim/colors
$ echo "Ahora crear enlace simbólico"
$ ./themesVim.sh
```

## Uso del Thema ##

*Vim* puede encontrar los temas, es más con la tecla *TAB* le mostrará cuales tiene
a su disposición.

Ejemplo trabajando en *Vim*:
```
:colorscheme TAB
```
Puede también si es de su preferencia, iniciar por defecto algún tema en *vim*;
escriba en su archivo *.vimrc* al final algo como esto:

```
colorscheme kalt
```

**nota**: no olvide que puede listar los temas que tiene a su disposición (nuevos)
en el directorio, para ello le muestro un ejemplo desde el propio *Vim*:

```
:!ls ~/.vim/colors/
```

## Agregar más temas ##

Si desea agregar más temas puede usar este enlace
http://bytefluent.com/vivify/


Att: Grupo pinguinoTux
