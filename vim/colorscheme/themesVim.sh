#! /bin/bash

echo "Hola `whoami`, este script crea un enlace simbolico al directorio de vim"
echo "el enlace apuntara a varios temas que puede usar para mejorar la apariencia de vim"

ln -sv `pwd`/colors ~/.vim/colors

echo "Enlace creado"

stat ~/.vim/colors

echo "Espero que sea de tu agrado `whoami`"
echo "Att: Johnny Cubides"
