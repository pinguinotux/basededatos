# Funciones de este VIMRC #

## Apuntes() ##

Esta función crea un formulario para tomar apuntes con LaTeX.

Esta función llamada por *call* desde **vim** hace una réplica del
archivo *Makefile*. Para que quede personalizado el proyecto, se pide
el nombre del proyecto para sustituir el nombre en el Makefile.
Se crea una copia del archivo apuntes.tex que se encuentra en la
base de datos. Por último ejecuta el Makefile con init dejando listo
para el uso correspondiente.

**nota:** El comando sed con el argumento busca coincidencias en la linea
1 hasta la 2 de la palabra apuntes en el archivo Makefile para remplazar
la cadena apuntes por el nuevo nombre.

## LaTex_InfoIEEE() ##

Ésta función es similar a *Apuntes()*; llamada desde vim con el comando
*call* permite traer del directorio de *LaTeX* una copia del documento
guía para los informes en formato IEEE, ejecuta el Makefile con el 
parámetro init para que el *make* cree una copia de todas las secciones
fuente src.

## LaTeX_imagIEEE() ##

Llamando ésta función con *call* desde **vim** puede agregar imágenes
a un documento *LaTeX*.

## LaTeX_colummBeamer() ##

La llamada de ésta función desde un archivo LaTeX que sea formulario
de una presentación en beamer, permite crear varias columnas.

## LaTeX_Tabla() ##

Ésta función llamada con el comando *call* desde **vim**, permite crear
tablas en archivos *LaTeX*.
