#! /bin/bash

echo "Hola `whoami`,"
echo "Este comando creará un enlace simbólico del archivo .vimrc
del usuario `pwd`"

echo "Escoja uno de los .vimrc a usar" 

ls -d */

read -p "digite cual: " -r vimrc

rm -v ~/.vimrc
ln -sv `pwd`/$vimrc/vimrc ~/.vimrc

echo "finalizado"
echo "Esperamos que sea de su agrado `whoami`"
echo "Att: Johnny Cubides"
echo "grupo pinguino Tux"
