# Instalar paquetes de forma manual en LaTeX#

## IEEEtrans ##
Para instalar el paquete *IEEEtrans* basta con descomprimir el .zip
en el directorio /usr/share/texmf/tex/latex para luego
instalarlo con el comando **texhash** como administrador.

Nota si no va hacer uso de IEEEtrantools.sty como de ejemplo:
```
\usepackage[retainorgcmds]{IEEEtrantools}
```
copie el archivo IEEEtran.sty en el directorio **/usr/share/texmf/tex/latex**
para luego ejecutar el comando **texhash**.


## xcolor ##

Descomprima el contenido del .zip en el directorio **/usr/share/texmf/tex/latex**
entre al directorio xcolor y ejecute el siguiente comando:
```
latex xcolor.ins
```
Finalice con el comando **texhash**

## listings ##

Descomprima el contenido del archivo.zip en el directorio **/usr/share/texmf/tex/latex**
dentro del directorio listing ejecute el Makefile con el comando **make**
termine con el comando **texhash**.

## Multirrow ##

Descomprima el archivo.zip en el directorio **usr/share/texmf/tex/latex** luego 
solo ejecute el comando **texhash** siempre como administrado.

## Cancel ##

Descomprima el archivo.zip en el directorio **usr/share/texmf/tex/latex** luego 
solo ejecute el comando **texhash** siempre como administrado.


## spanish ##

sudo latex spanish.ins
sudo texhash

## base ##

por sí solo no es suficiente


## moderncv ##

Instalar sólo si se hacen hojas de vida

Descomprima el archivo.zip en el directorio **usr/share/texmf/tex/latex** luego 
solo ejecute el comando **texhash** siempre como administrado.

nota: puede depender de etoolbox.

## beamer theme black kmbeamer##

Temas de beamer oscuros y demás

### Instalación ###
sudo cp kmbeamer /usr/share/texmf/tex/latex/
cd /usr/share/texmf/tex/latex
sudo unzip kmbeamer.zip
cd kmbeamer/
**sudo texhash**


## etoolbox ##

<<<<<<< HEAD
## iftex ##

Documentación amforth requere

Descomprima el archivo.zip en el directorio **usr/share/texmf/tex/latex** luego 
solo ejecute el comando **texhash** siempre como administrado.
=======

## gensymb ##

Descomprima el contenido del was.zip en el directorio **/usr/share/texmf/tex/latex**
entre al directorio was y ejecute el siguiente comando:
```
latex gensymb.ins
```
Finalice con el comando **texhash**
>>>>>>> 28939088c1a28d5ad3f56f4e68095404eee10626


