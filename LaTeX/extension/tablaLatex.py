#! /usr/bin/python
# -*- coding: utf-8 -*-

from Tkinter import *
import os
import subprocess

# Metodo para la funcion salir 
def cancelar():
    os.system('rm /tmp/tabla.tex')
    os.system('rm /tmp/vistaPrevia.tex')
    os.system('rm /tmp/vistaPrevia.pdf')
    exit()

#########################################################################
def continuarOK():
    os.system('rm /tmp/tabla.tex')
    save_file('/tmp/tabla.tex', ajustar())
    exit()

#########################################################################
def mostrarVistaPrevia():
    os.system('rm /tmp/vistaPrevia.tex')
    os.system('rm /tmp/vistaPrevia.pdf')
    nuevaVistaPrevia = "\\documentclass{article}\n"\
            + "\\usepackage{graphicx}\n"\
            + "\\begin{document}\n" \
            + ajustar() + "\\end{document}\n"
    save_file('/tmp/vistaPrevia.tex', nuevaVistaPrevia)
    os.system('vprerex /tmp/vistaPrevia.tex')

#########################################################################
def save_file(fileName, tablaInsert):
    f = open(fileName, 'w')
    f.write(tablaInsert)
    f.close

#########################################################################
def ajustar():
    beginTabla = "\\begin{table}[h!]\n" + "\\centering\n"
    if checkVar1.get() == 1:
        beginTabla = beginTabla + "\\resizebox{" + E1.get() + "cm}{!} {\n"
    beginTabla = beginTabla + dimensionar()
    if checkVar1.get() == 1:
        beginTabla = beginTabla + "}\n"
    beginTabla = beginTabla + "\caption{}\n" + "\\label{}\n" + "\\end{table}\n"
    return beginTabla

#########################################################################
def dimensionar():
    capTabla1 = miText.get(1.0, END) # Es flotante ??
    counter = 0
    for caracter in capTabla1:
        if caracter == '\t':
            counter = counter + 1
        elif caracter == '\n':
            break
    respuesta = "\\begin{tabular}{|c|"
    for x in range(counter):
       respuesta = respuesta + "c|"
    respuesta = respuesta + "} \\hline\n" + tablaLatex() + "\\end{tabular}\n"
    return respuesta

#########################################################################
# Methodo usado por el boton continuar.
def tablaLatex():
    espaciador = ""
    if varRadioButton.get() == 1:
        espaciador = '\t'
    elif varRadioButton.get() == 2:
        espaciador = ' '
    elif varRadioButton.get() == 3:
        espaciador = EOtraOpcion.get()
    print "aqui"
    print varRadioButton.get()
    print espaciador
    print "fin"
    # Toma el string de la caja de texto desde principio a fin
    capTabla = miText.get(1.0, END) # Es flotante ??
    temp = ""   # Variable para modificar el caoTabla
    counter = 0 # Counter de los \n
    for letra in capTabla:  # procedimiento contador de \n
        if letra == '\n':
            counter = counter + 1
    #print counter
    if counter != 0: # Si no hay \n no podrá generar tabla
        temp_N = 0   # Variable comparadora de \n
        for letra in capTabla:
            if letra == espaciador:
                temp = temp + '\t&'
            elif letra == '\n':
                temp_N = temp_N + 1
                if temp_N == 1 or temp_N == counter:
                    temp = temp + '\t\\\\\hline\n'
                else:
                    temp = temp + '\t\\\\\n'
            else:
                temp = temp + letra
    #print temp_N
    print(capTabla)
    print(temp)
    return temp;

#############################################################################
# Crear ventana y frame
root = Tk()
frameRight = Frame(root)
frameRight.pack(side = RIGHT)
frameLeft = Frame(root)
frameLeft.pack(side = LEFT)
# Modificar la vetana
#           "AnchoxAlto"
root.geometry("765x380")

###############################################################################

# Crear caja de texto e insertar en root
#                   columnas,   filas
miText = Text(frameLeft, width=80, height=26)
miText.pack()

################################################################################
# Botón check
checkVar1 = IntVar()
c1 = Checkbutton(frameRight, text="Ajustar tabla al documento", \
        variable=checkVar1, onvalue=1, offvalue=0)
c1.pack()


# Insertar valor del ajuste
L1 = Label(frameRight, text="Valor del Ajuste en cm:↓↓↓")
L1.pack()
E1 = Entry(frameRight)
E1.pack()

#################################################################################
#Tipo de separador
labelSeparador = Label(frameRight, text="Tipo de separador:")
labelSeparador.pack()
# Radio Button
varRadioButton = IntVar()
#varRadioButton = 1
opcionRB1 = Radiobutton(frameRight, text="Tabulador \\t", variable=varRadioButton, \
        value=1)
opcionRB1.select() # Iniciar con opcion 1 seleccionada
opcionRB1.pack()
opcionRB2 = Radiobutton(frameRight, text="Espaciador  ", variable=varRadioButton, \
        value=2)
opcionRB2.pack()
opcionRB3 = Radiobutton(frameRight, text="Otra:↓↓↓     ", variable=varRadioButton, \
        value=3)
opcionRB3.pack()
EOtraOpcion = Entry(frameRight)
EOtraOpcion.pack()
#################################################################################

botonVistaPrevia = Button(frameRight, text="Vista Previa", command=mostrarVistaPrevia)
botonVistaPrevia.pack()

# Crear boton continuar
botonOK = Button(frameRight, text="Continuar", command=continuarOK)
botonOK.pack(side = LEFT)
# Crear boton cancelar
botonCancelar = Button(frameRight, text="Cancelar", command=cancelar)
botonCancelar.pack(side = RIGHT)

#################################################################################

# Lanzar ventana principal
root.mainloop()
