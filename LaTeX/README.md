# Mensajes de error en LaTeX no comunes #

**! LaTeX Error: Too many unprocessed floats:** Es posible a que se deba a la dificultad
de poner varias imágenes flotantes en una misma página.

**Solución:**

poner el comando antes de la línea de la siguiente imagen para iniciar
en nueva página.
```
%documento Latex
%...
\clearpage
%...
```
**nota:** Tratar de dejar entre 4 o 5 imágenes por página, es decir,
el comando escribirlo cada cierta cantidad de imagenes
