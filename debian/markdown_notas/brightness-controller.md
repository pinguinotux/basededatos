# Instalar brightness-controller en Debian Buster con LXDE #

Para quienes usamos LXDE como entorno de escritorio lidiamos con
algunas dificultades con respecto a configuraciones personalizadas.

Tal es el caso de **cambiar el brillo de nuestra pantalla** entre otros,
Para el caso particular cuando deseamos hacer tal cambio, podemos hacer
uso de comandos por consola como el que sigue:

```sh
xrandr --output LVDS-1 --brightness 0.6
```

En el anterior comando, brightness nos define el valor del brillo donde su
argumento es un valor en el intervalo [0:1], en el ejemplo es de 0.6 (o 60 % 
del brillo).

Existe una herramienta muy útil llamada 
[brightness-controller](https://github.com/lordamit/Brightness) que al 
instalarla nos facilitará éste proceso, sin embargo, la dependencia **python-pyside** 
para **Debian Buster** no se encuentra en los repositorios precompilados.

A continuación se explicará una *altenativa* de instalación.

## Instalación de dependencias ##

* Abra una terminal, debe saber usar los siguientes comandos: **cd**, **ls**

* Abra con permisos de superusuario el archivo de configuración **sources.list**

Ejemplo:

```sh
sudo nano /etc/apt/sources.list
```

* Al final del fichero copie el siguiente contenido:

```sh
deb http://ftp.de.debian.org/debian sid main
```

* Guarde los cambios

* Actualice la información de las fuentes de precompilados.

```sh
sudo apt update
```

* Instale la dependencia **python-pyside**

```sh
sudo apt install python-pyside
```

* **Recomendación**: Finalizada la instalación comente la línea que agregó 
al final de **sources.list**.

```sh
\# deb http://ftp.de.debian.org/debian sid main
```

* **Recomendación**: Haga otro update a la lista gestor de paquetes.

`
sudo apt update
`

## Instalación de brightness-controller #

* Verifique que tenga instalado git
En caso de no tenerlo instalado: **sudo apt install git**

* Clone el repositorio de brightness-controller

```sh
git clone https://github.com/LordAmit/Brightness.git
``` 

* Entre al directorio y ubíquese aquí:

```sh
Brightness/src/
```

* Cree un enlace simbólico a la aplicación:

```sh
sudo ln -s `pwd`/init.py /usr/local/bin/brightness-controller
```

## Abrir la aplicación ##

* Desde una terminal puede ejecutar el siguiente comando:

`brightness-controller &`

END

Johnny Cubides
