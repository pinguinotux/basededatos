.. notasLinux documentation master file, created by
   sphinx-quickstart on Sun Aug 21 10:28:50 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to notasLinux's documentation!
======================================

.. toctree::
    
    soluciones/index_soluciones.rst
    programas/index_programas.rst
    electronica/index_electronica.rst
    ajustes/index_ajustes.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

