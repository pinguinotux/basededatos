.. _soluciones-index:

Problemas y Soluciones
======================

.. toctree::
    :maxdepth:  1
    :numbered:

    teclado.rst
    quitarBeep.rst
    chromium.rst
    virtualbox.rst
    debianUpgrade.rst
    recuperarArchUSB.rst
