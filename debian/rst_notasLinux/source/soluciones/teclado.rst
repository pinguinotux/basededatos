Configuraciones de teclado
==========================

Configurar idioma español-english
---------------------------------

Para poder cambiar la region del teclado se debe tener instalado **setxkmap**

.. code:: shell 

    $ setxkmap es #Cambia a teclado Español
    $ setxkmap us #Cambia a teclado americano
