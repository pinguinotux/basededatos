Chromium
========

Extensiones no son instalables en modo desarrollador debian 9
-------------------------------------------------------------

Aún no he estado al tanto del error, pero una manera de resolverlo la
encontré en la siguiente fuente:

[https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=852398](Fuente)

Copying this over from #851692. A method of enabling extensions without
re-compiling Chromium. Not saying it is ideal

1. Find your extensions:
They are in $XDG_CONFIG_DIR/chromium/<profile>/Extensions/<hash>/<version>

For example, uBlock is at
/home/alok/.config/chromium/Default/Extensions/cjpalhdlnbpafiamejdnhcphjbkeiagm/1.9.4_0

2. Create a file in /etc/chromium.d/enable-extensions with the content
export CHROMIUM_FLAGS="$CHROMIUM_FLAGS --load-extension=<extension-path
from above>"

3. You have to create multiple lines of the form above for each profile.
Note that you will have to update this file when the extension version
changes. If you have the same extension in multiple profiles, you will have
make explicit entries for it as per step 2.
