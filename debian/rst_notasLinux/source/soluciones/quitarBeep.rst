Quitar Beep del Sistema
=======================

El Beep que se da por default al iniciar una distro Debian
(No siempre) se puede silenciar por sesión o por boot.

Quitar Beep por sesión
----------------------

Si se requiere que solo sea una vez se ejecuta el
siguiente comando:

.. code:: shell

    # rmmod pcspkr


Quitar Beep de manera permanente 
--------------------------------
    
Pero si se quiere silenciar cada vez que se inicie el
núcleo se debe hacer lo siguiente:

Acceder al archivo **fbdev-blacklist.conf** con el editor
de texto de su preferencia.

Ejemplo:

.. code:: shell
        
    # vim /etc/modprobe.d/fbdev-blacklist.conf

agregar la siguiente línea al final del archivo:

.. code:: shell

    blacklist pcspkr
        
