Debian upgrade
==============

.. code-block:: sh

    apt-get clean
    apt-get update
    apt-get diskupgrade

Crear apt sourceslist para debain
=================================

La generación de estos sources list, permitirá seleccionar de forma
automática la mejor opción.

`https://debgen.simplylinux.ch/ <https://debgen.simplylinux.ch/>`_
