Recuperar archivos borrados de memoria USB
==========================================

`fuente <https://blog.desdelinux.net/recuperar-archivos-borrados-facilmente-con-photorec-desde-la-consola/>`_

Instalación de paquete de recuperación
++++++++++++++++++++++++++++++++++++++

**Pasos Instalación:**

1. Ejecutar el siguiente comando

   .. code-block:: sh

       sudo apt-get install testdisk

Con el anterior paquete obtendrá dos programas; **TestDisk** y **PhotoRec**.
Mientras TestDisk se especializa en la recuperación de *particiones perdidas*, PhotoRec lo hace
para la recuperación de archivos borrados.

Se usará PhotoRec.

Recuperación de Archivos borrados con PhotoRec
++++++++++++++++++++++++++++++++++++++++++++++

**Pasos de recuperación:**

1. Conectar pendrive (memoria usb).

2. Crear un directorio dónde almacenar la información recuperda.

3. Ejecutar el programa PhotoRec con permisos de superusuario.

   .. code-block:: sh

       sudo photorec

4. Seleccionar la memoria (también se puede en discos) a revisar, para luego
   darle Enter y proceder.

5. Seleccionado el disco o memoria, deberá seleccionar la partición (si existe más de una) en
   la cual quiere hacer la recuperación de archivos.

6. Seleccionar el tipo de partición (no la partición) donde fueron borrados los archivos,
   Esto permitirá ayudar a PhotoRec en la búsqueda.

7. Ahora tendrá dos posibilidades **Free**, **Whole**; en Free recuperará solo lo que esté borrado en el espacio libre, en Whole buscará y recuperará lo borrado y lo no borrado.

8. Le solicitará 
