.. _ajustes-index:

Ajustes y Configuraciones
=========================

.. toctree::
    :maxdepth:  1
    :numbered:

    services.rst
    openbox.rst
