Services
========

Iniciar, detener y reiniciar services
-------------------------------------

Inicialmente para saber qué servicios tiene el equipo en ese momento
ejecutar el siguiente comando:

.. code:: sh

    # service --status-all 

1. Los servicios que aparecen con un signo (-) son servicios inactivos.

2. Los servicios que están acompañados con un signo (+) son servicios activos.

Para **iniciar** un servicio:

.. code:: sh

    # service nombreServicio start


Para **detener** un servicio:

.. code:: sh

    #  service nombreServicio stop

Para **reiniciar** un servico:

.. code:: sh

    # service nombreServicio restart


Crear un Servicio
-----------------

Los servicios son útiles ya que permiten automatizar tareas
que deseamos estén activas siempre que se inicie nuestro equipo,
como por ejemplo iniciar una red wifi entre otros.

La forma general de crear un servicio es crear un script de shell (en general bash),
alojarlo en un directorio específico y darle permisos de ejecución.

Script
++++++

El script por ser ahora un servicio del sistema se nombra en general (como si se 
tratara de una normalización) con la primera letra **S**, luego el número de 
servicio que queramos y por último el nombre del programa a ejecutar. Ejemplo:

.. code:: sh

    # vim S200miPrograma

.. note::

        Dése cuenta que no necesariamente debe tener la extensión **.sh** para
        que sea ejecutado el script por el shell, además, el nombre es solo
        una norma para que se sepa que tipo de archivo estoy usando, como 
        si se tratara de una guía.

El script debe tener la estructura de cualquier otro script para un shell,
como en el siguiente ejemplo:

.. code:: sh
    
   #! /bin/bash
   #
   # Un comentario del servicio y de lo que espera que haga
   #

   aplicacion -argumentos directorio/ &
    
.. note::

        Fíjese el uso del ampersand (**&**) al final de la línea, esto se
        hace para que cuando se ejecute el servicio libere el shell (ejecutando
        el programa background) y así entregando el prompt para nuevas instrucciones.


ubicación y permisos del servicio
+++++++++++++++++++++++++++++++++

Una vez terminado el de crear el script, será alojado en el siguiente directorio
de la tabla de servicios del S.O. Ejemplo:

.. code:: sh

    # mv S200miPrograma /etc/init.d/

Finalmente debe darle permisos de ejecución al script. Ejemplo:

.. code:: sh

    # chmod +x S200miPrograma


Con esto, el servicio estará listo para iniciarse en un nuevo arranque del S.O.. 
Puede probar su servicio ejecutando los comandos de iniciar, detener y reiniciar 
servicio.

        

        
