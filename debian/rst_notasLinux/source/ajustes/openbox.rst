Configurando OpenBox
====================

Para poder configurar OpenBox

Configuración de teclado ATAJOS
+++++++++++++++++++++++++++++++

Para saber con cual tecla se quiere hacer un atajo se puede hacer lo que sigue:

en una terminal ejecutar el comando eve

        $ eve

En el archivo rc.xml que en general se encuentra en ~/.config/openbox buscar los
keybind y ajustarlo a la necesidad.

Ejemplo:

.. code-block:: xml

        <keybind key="W-s">
            <action name="Execute">
                    <command>setxkbmap es</command>
            </action>
        </keybind>
        <keybind key="W-i">
            <action name="Execute">
                    <command>setxkbmap us</command>
            </action>
        </keybind>
        </keyboard>

Explicación:

keybind -> Se oprime la tecla window + s/i
command -> El commando a ejecutar es cambiar la configuración del teclado de
ingles (setxkbmap es) a español (setxkbmap us) y viceversa.

