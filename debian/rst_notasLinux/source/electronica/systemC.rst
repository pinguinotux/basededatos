SystemC para verificación de sistemas
=====================================

Instalación:
------------

Dirigirse a la siguiente dirección http://accellera.org/downloads/standards/systemc y
ubique la siguiente librería:

- SystemC 2.3 (Includes TLM)

--------------

Descargar, desempaquetar y entrar al directorio, ejemplo:

.. code:: shell

    $ tar xvf systemc-2.3.1.tgz
    $ cd systemc-2.3.1/

-------------

Crear directorio para librería, crear directorio para compilación, Configurar librería, 
construir librería, revisar construcción e instalar,  ejemplo:

.. code:: shell

    # mkdir /usr/local/systemc-2.3.1/
    $ mkdir objdir; cd objdir
    # ../configure --prefix=/usr/local/systemc-2.3.1/
    $ make check
    # make install

Makefile
---------------

Crear Makefile para ejecución de proyectos

.. code:: shell
    
    #make pro=proyecto.cpp

    dir=/usr/local/systemc-2.3.1/
    arch=lib-linux/

    compile: ${pro}
	    g++ ${pro} -o executablemain -I. -I $(dir)include/ -L. -L $(dir)$(arch) -lsystemc -lm -L. -L $(dir)$(arch)libsystemc.so -lm -pthread -Wl,-rpath -Wl,$(dir)$(arch)

    export_variable_entorno:
	    export SYSTEMC_HOME=$(dir)

    clean:
	    rm executablemain


Compilación
-----------

Para compilar un **proyecto.cpp** se debe hacer lo siguiente:

1. Exportar variables de entorno:

   .. code:: shell

        make export_variable_entorno
       
2. Compilar proyecto.cpp
   
    .. code:: shell

        make pro=proyecto.cpp


Bugs
-----

error: ‘std::gets’ has not been declared
++++++++++++++++++++++++++++++++++++++++

Es debido a que el gets fue retirado del compilador ya en la versión g++11.
La solución consiste en ubicar el archivo **systemc.h** y comentar la línea

Ejemplo:

.. code:: sh

    # cd /usr/local/systemc-2.3.1/include
    # vim systemc.h
    118 //      using std::gets;
    Guardar y salir        
