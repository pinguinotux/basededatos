ngspice: Simulador de ciruitos eléctricos
=========================================

Colores para el plot de variables
---------------------------------

`plot <http://www.h-renrew.de/h/ngspice/ngspicedemo.html>`_

Snippet en VIM:
---------------

El snippet siguiente facilita la entrada de parámetros para desarrollar
simulaciones en ngspice, ya que ngspice es un programa que permite 
las simulaciones dese consola más que desarrollar un fronter, es usar
las ventajas en flexibilidad que permite ngspice.

Para que el snippet mencionado funcione en vim, éste debe ser agregado
a la lista de snippet de vim que generalmente se encuentra en la siguiente
dirección:

.. code:: sh 

    $ cd ~/.vim/snippet/

.. note::

    No olvide que debe primero tener instalado el plugins de snippet para VIM
    denominado **snipMate**

Copie el siguiente archivo `sim.snippets`_ y péguelo en el directorio de snippets de VIM

.. _sim.snippets: sim.snippets

.. note::

        También puede encontrar el sim.snippet en el repositorio de Bitbucke
        `archivo_Bitbucket`_
        
        .. _archivo_Bitbucket: Bitbucket.org

Uso del snippet en VIM:
+++++++++++++++++++++++

Para que funcione el snippet en VIM, inicialmente debe hacer estos pasos:

1. Crear un archivo con extensión **sim**, ejemplo:

   .. code:: sh
        
        $ vim archivo.sim

2. Para habilitar el snippet ejecute el siguiente comando dentro de VIM:

   .. code:: vim

        :set ft=sim

3. Ya activado el snippet, basta hacer uso de las palabras declaradas dentro
del snippet para hacer uso de las funciones de simulación. Recuerde que se
escribe la palabra declarada en el snippet y seguidamente tabular (TAB) para que
escriba las instrucciones completas. Recuerde también que solo tabulando (TAB o SHIFT
+TAB) puede recorrer las variables e ir rellenando cada una de ellas sin oprimir
ENTER u otra tecla. Ejemplo:

    .. code:: vim

        tran<tab>

Generará:

    .. code:: vim

        tran step stop start

Donde step es la variable 1, stop es la variable 2, y start es la variable 3, las
cuales se pueden recorrer con TAB, según el código del snippet, véase el código:

    .. code:: vim

        snippet tran
            tran ${1:step} ${2:stop} ${3:start}


Funciones ngspice en el snippet
+++++++++++++++++++++++++++++++

* **begin**: Crea el entorno de simulación.

* **option**: Permite definir el formato de salida de las variables en el postscript

* **tran**: Puede generar los pasos necesarios para una simulación transitoria.
  
  *Variables*:
    * **step**: pasos en tiempo de la simulación ejemplo: 1u, 1m.
    * **stop**: Tiempo de finalización de la simulación (superior o múltiplo de 
      **step**).
    * **start**: Permite definir desde qué tiempo empieza la simulación, (tiempo
      inferior a **stop**), es opcional (es decir, lo puede quitar de la instrucción
      *nota*: recuerde que los valores deben ser proporcionales, ejemplo:
      tran 1u 1m 0

* **plotvX**: Donde *X* puede ser del 1 al 6, permite visualizar el estado de las
  variables de voltaje en pantalla y generar un postscript (no olvidar el option).
  No olvidar que es necesario especificar el nodo donde se quiere ver el voltaje.

  *Variables*:
    * **nX**: Donde nX es el nodo al cual se le desea visualizar la tensión asociada.

* **sweeprvX**: Donde *X* puede ser del 2 al 6.
