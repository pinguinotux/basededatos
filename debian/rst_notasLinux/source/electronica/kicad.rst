Kicad
=====

Intsalación
+++++++++++

.. code-block:: sh

        sudo apt-get install kicad

Librerías
+++++++++

En la instalación de librerías y para que queden compartidas
con los demás usuarios,

1. Abrir el wizard manager de librerías del **PCBnew**

2. Seleccionar en Preferences -> Footprint Library Wizard.

3. Seleccionar la opción GitHub repository.

4. Hacer una copia local asegurándose que quede en su directorio
   /usr/share/kicad/modules/

5. En la siguiente pestaña instalar todos (seleccionar todo).

Con ésto será suficiente para tener los footprint y estar asociados para
los proyectos siguientes.
