wget
====

Descargar archivo
-----------------

Para descargar un archivo desde la web basta hacer el siguiente comando.

.. code:: shell
    
    $ wget archivo
        

Descargar página web
--------------------

Para descargar página web con sus respectivos links para otras páginas
junto estas mismas (Como si se tratara de árboles de páginas) se puede hacer
uso del siguiente comando:

.. code:: shell
   
    $ wget --recursive --level=20 -p --convert-links direccionHtml 
       
EL **level** puede ser de otro valor, eso indica la "recursividad" de
los links y directorios que estarán conectados.

