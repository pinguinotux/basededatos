QUCS
====

Instalación de QUCS en Linux
++++++++++++++++++++++++++++

git clone --recursive https://github.com/Qucs/qucs
git clone https://github.com/Qucs/ADMS

sudo apt-get install build-essential libqt4-dev libqt4-qt3support automake libtool gperf flex bison texlive-font-utils texlive-full octave

descargar e instalar octave-esptk
https://packages.debian.org/stable/math/octave-epstk
sudo dpkg -i octave-epstk.deb (ejemplo)

entrar ADMS

./bootstrap.sh

./configure

make

Crear enlaces simbolicos

sudo ln -s `pwd`/admsCheck /usr/local/bin/

sudo ln -s `pwd`/admsXml /usr/local/bin/

entrar a qucs

./bootstrap.sh

./configure

make

sudo make install

Instalación de Qucs desde los precompilados de Ubuntu
+++++++++++++++++++++++++++++++++++++++++++++++++++++

Se debe descargar el precompilado .deb que se encuentra en el repositorio:

`qucs0.18 64Bits <https://launchpad.net/~qucs/+archive/ubuntu/qucs/+files/qucs_0.0.18-2_amd64.deb>`_<`0`>

`qucs0.18 32Bits <https://launchpad.net/~qucs/+archive/ubuntu/qucs/+files/qucs_0.0.18-2_i386.deb>`_

Ahora dependiendo de la architetura se instala uno o el otro; la instalación se hace como sigue:

.. code-block:: sh

    sudo dpkg -i qucs_x_.deb


Donde x es la versión de qucs a instalar.

Finalmente instalar el siguiente paquete modulo de gtk.

.. code-block:: sh

     sudo apt install libcanberra-gtk-module

**END**
