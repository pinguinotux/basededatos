GoldeDict
=========

apt-get install goldendict

apt-get install dict-freedict-eng-spa

Fuentes diccionarios Babylon
++++++++++++++++++++++++++++

`Babylon <http://www.babylon-software.com/free-dictionaries/>`_

Descargar los diccionarios que se necesiten, guardarlos en un directorio
para luego enlazarlos a goldendict como sigue:

Edit->Diccionaries->Files-Adds... Agregar el path del directorio donde están
los diccionarios, activar la pestana de **recursive**, finalmente **Rescan now**
, aplicar cambios y salir.

USO
+++

GoldeDict se puede ejecutar en segundo plano, para ver la traducción de las
palabra que se desee, señale con el cursor la palabra y como se hace para copiar
en el portapapeles (Ctrl+C) hacerlo dos veces seguidas; con ello saldrá si 
existe la palabra la traducción al lenguaje deseado. Al retirar el mose de la
ventana que emerge, esta ventana, nuevamente se oculta.

Wikipedia
+++++++++

Español
-------

`Spanish Wikipedia <https://es.wikipedia.org/w>`_
`Spanish Wiktionary <https://es.wiktionary.org/w>`_
