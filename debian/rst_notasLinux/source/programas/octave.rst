Octave
======

Instalar librería de control
----------------------------

1. Instalar librerías de desarrollador.

.. code:: sh

    # apt-get install liboctave-dev

2. Descargar paquete de control.

   control-3.0.0.tar.gz

3. Instalación de control en octave:

.. note::

        Tener en cuenta dirección relativa del paquete a
        instalar.

4. Ejecutar como administardor octave-cli. Ejemplo:

.. code:: sh

    # octave-cli

    
5. Instalar paquete de control. Ejemplo:

.. code:: sh

   > pkg install "control-3.0.0.tar.gz"

**Opcion:** Si se desea ver el proceso de instalación de una
librería pruebe ejecutando el siguiente comando:

.. code:: sh

        > pkg install "control-3.0.0.tar.gz" -verbose

https://www.gnu.org/software/octave/doc/v4.0.1/Installing-and-Removing-Packages.html
END
