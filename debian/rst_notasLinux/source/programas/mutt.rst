MUTT, Cliente de correo
=======================

Instalación
-----------

Se debe descargar primero el archivo fuente de MUTT

Asegurarse de tener instaladas las siguientes dependencias:

* libncurses-dev
* libssl-dev
* libsasl2-dev
* libdb-dev

Descomprimir la fuente

Ejecutar ./config con las siguientes banderas:

./configure 
--enable-pop 
--enable-smtp 
--enable-imap 
--enable-mailtool 
--with-ssl 
--with-sasl 
--enable-sidebar 
--enable-external-dotlock 
--with-curses
--enable-hcache  // Permite almacenar información en la cache (Precarga)


Construir:

make

Instalar 

.. code-block:: sh

    sudo make install
