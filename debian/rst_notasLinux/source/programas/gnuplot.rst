GNUPLOT
=======

Ejemplos de uso
+++++++++++++++

.. code-block:: gnuplot

      R1 = 240
      Iadj = 50E-6
      Vref = 1.25
      plot [100:2000] Vref*(1+x/R1)+(Iadj*x)

