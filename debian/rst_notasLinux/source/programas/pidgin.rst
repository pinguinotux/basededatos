Pidgin
======

Complementos
++++++++++++

pidgin-themes

Cuentas Telegram
----------------

`telegram-purple <https://github.com/majn/telegram-purple.git>`_

cuentas google:
---------------

Poner usuario y contraseña normal y en la pestaña Advance, en connect to server
poner lo siguiente **talk.google.com**.

Cuentas Facebook
----------------

`purple-facebook <https://github.com/dequis/purple-facebook>`_

O puede desacargar un precompilado desde aquí.

`precompilados <https://build.opensuse.org/package/binaries/home:jgeboski/purple-facebook?repository=Debian_8.0>`_

poner el nombre de cuenta dado por facebook no la cuenta de correo.

Cuentas de Whatsapp
-------------------

clonar y compilar el siguiente repo

`whatsapp-purple <https://github.com/davidgfnet/whatsapp-purple>`_

dependencias para compilación:

libfreeimage-dev

libprotobuf-dev

protobuf-compiler

protobuf-c-compiler

1. Compilar e instalar

make ARCH=x86_64

make install (permisos de administrador)

conseguir password pendiente...

https://github.com/tgalal/yowsup/wiki/yowsup-cli#your-login-credentials

https://github.com/davidgfnet/whatsapp-purple/issues/5

https://github.com/davidgfnet/whatsapp-purple/issues/19


http://huntingbears.com.ve/usando-whatsapp-desde-la-comodidad-de-tu-escritorio-con-pidgin.html

http://huntingbears.com.ve/utilizando-yowsup-para-obtener-las-credenciales-de-tu-usuario-en-whatsapp.html

Corrector Ortografico
+++++++++++++++++++++

Teniendo instalado **myspell-X** donde **X** es el diccionario del idioma a 
soportar basta con hacer clic en la entrada (donde redacto el mensaje a 
enviar a mi contacto seleccionado) y seleccionar el idioma con el que se
quiere corregir.

Ejemplo de instalación:

.. code-block:: sh

        sudo apt-get install myspell-es

 
