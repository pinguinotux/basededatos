apt-get
=======

Gestor de paquetes precompilados para una distribución.

apt-get install paquete

apt-get search paquete

apt-get purge paquete

apt-cache policy paquete
