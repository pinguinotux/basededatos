CONVERT
=======

Convertir imágenes en un archivo pdf
++++++++++++++++++++++++++++++++++++

Ejemplo:

.. code-block:: sh

    convert imag1 imag2 imag3 archive.pdf
