HIPCHAT
=======

Instalación
+++++++++++

Fuente: https://www.hipchat.com/downloads#linux

Antes debe comprobar que tenga instalado lo siguiente:

apt-transport-https

para ello ejeute el siguiente comando en la consola:

sudo apt-get install apt-transport-https

Y luego, como explica la fuente ejecutar el siguiente comando:

sudo sh -c 'echo "deb https://atlassian.artifactoryonline.com/atlassian/hipchat-apt-client $(lsb_release -c -s) main" > /etc/apt/sources.list.d/atlassian-hipchat4.list'
wget -O - https://atlassian.artifactoryonline.com/atlassian/api/gpg/key/public | sudo apt-key add -
sudo apt-get update
sudo apt-get install hipchat4 

Requerimientos Network
++++++++++++++++++++++

**Fuentes**

`Requerimientos Network <https://www.hipchat.com/help/networking>`_

`Prueba de puerto <https://confluence.atlassian.com/hipchatkb/unable-to-sign-in-to-the-hipchat-desktop-app-751436202.html>`_

Para comprobar que el puerto no esté cerrado por la compañía de servicio de 
Internet, pruebe lo siguiente en una terminal.

.. code-block:: shell

        telnet chat-main.hipchat.com 5222

Issues
++++++

1. Unable to access hipchat.com. Please check that your network is connected and try again

warning: [14:04:58][qt.network.ssl][:0] QSslSocket: cannot call unresolved function CRYPTO_num_locks
Warning: [14:04:58][qt.network.ssl][:0] QSslSocket: cannot call unresolved function CRYPTO_set_id_callback
Warning: [14:04:58][qt.network.ssl][:0] QSslSocket: cannot call unresolved function CRYPTO_set_locking_callback
Warning: [14:04:58][qt.network.ssl][:0] QSslSocket: cannot call unresolved function ERR_free_strings

**Solución**

fuente: https://confluence.atlassian.com/hipchatkb/hipchat4-debian-client-doesn-t-connect-859526103.html

sudo ln -s /usr/lib/x86_64-linux-gnu/libssl.so.1.0.2 /opt/HipChat4/lib/

sudo ln -s /opt/HipChat4/lib/libssl.so.1.0.2 /opt/HipChat4/lib/libssl.so
