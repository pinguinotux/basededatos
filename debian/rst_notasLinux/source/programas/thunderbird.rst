Thunderbird
===========

Actualización
+++++++++++++

Basta con ir a menú -> ayuda -> Acerca de Thunderbird;
se abrirá una ventana emergente y en ésta Thunderbird
comprobará si hay actualizaciones, si las hay las
descargará y luego se instalarán, finalmente reiniciar
Thunderbird.

Instalación de tema Dark
++++++++++++++++++++++++

Descargar el tema

`TT DeepDark <https://addons.mozilla.org/en-US/thunderbird/addon/tt-deepdark/>`_

Ejecutar Thunderbird e ir a Administrador de complementos -> Extensiones; y en el botón 
de configuraciones, escoger la opción Instalar complementos desde archivo.., allí
buscar y seleccionar el archivo con la extensión .xpi para finalmente reiniciar thunderbird.
