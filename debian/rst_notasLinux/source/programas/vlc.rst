VLC
===

Ver vídeos de youtube
+++++++++++++++++++++

Si desea ver vídeos de youtube sin los molestos comerciales intentar esto:

Existe un script de lua que permite ejecutar estas tareas
sin embargo, en Debian no viene instalado por defecto.

Para instalarlo basta con hacer lo siguiente.

sudo apt-get install vlc-plugin-access-extra
