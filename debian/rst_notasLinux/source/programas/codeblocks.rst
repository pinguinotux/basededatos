CodeBlocks
==========

Agregar Color Themes
++++++++++++++++++++

1. Descargar y guardar los *Color Themes* Los cuales están en el siguiente link:
   `Themes_CodeBlocks`_. Guardar el archivo con el nombre **ColorThemes.conf**.
   
    .. _Themes_CodeBlocks: http://wiki.codeblocks.org/index.php?title=Syntax_highlighting_custom_colour_themes

2. Ejecutar el comando en consola **cb_share_config** sin correr codeblocks.

.. image:: cbShareConfig.png

Como se muestra en la anterior imagen seleccionar a la izquierda los temas que
serán guardados en el **default.config**; Finalmente transferir y guardas.

3. Seleccionar el tema para la sintaxis como se ve en la siguiente imagen:

   .. image:: highlighting.png
