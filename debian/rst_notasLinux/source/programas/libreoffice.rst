LibreOffice
===========

`fuente <https://blog.desdelinux.net/instalar-corrector-ortografico-y-gramatical-en-libreofficeopenoffice/>`_

No olvide que para las instalaciones, si quiere ver los cambios en la mayoría
de casos deberá reiniciar libreoffice.

Corrector ortográfico
+++++++++++++++++++++

Basta con instalar el lenguaje de su elección desde los repositorios
ejemplo:
**Español** -> sudo apt-get install myspell-es

LanguageTool
+++++++++++++++++++++++++++

Corrector gramatical (reglas de un lenguaje)

Instalación
-----------

Descargar la extensión desde el sitio oficial

`LanguagueTool <https://extensions.libreoffice.org/extensions/languagetool>`_

Instalar en administrador de extensiones como un archivo.

**nota1**: Si presenta problemas comprobar que se tenga instalado
libreoffice-java-common



