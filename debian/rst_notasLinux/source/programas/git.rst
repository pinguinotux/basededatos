GIT
===

Ignorando archivos
------------------

Crear el archivo .gitignore. Ejemplo:

$ touch .gitignore

Allí crear la lista de archivos que desea que sean ignorados y no ser
sincronizados con el repositorio remoto. Ejemplo:

build/

En el ejemplo todo lo que este dentro de build y cualquier coincidencia con ese
mismo nombre será ignorado.

Nota: cuando se crea el repositorio en bitbucket hay una pestaña que se puede activar
para que genere automáticamente el archivo .gitignore.

Además si lo hace manualmente durante un proceso, donde ya se tenga en cuenta reglas
es posible que deba hacer lo siguiente, como es indicado en la siguiente fuente_:

.. _fuente: https://help.github.com/articles/ignoring-files/

1. In Terminal, navigate to the location of your Git repository.

2. Enter touch .gitignore to create a .gitignore file.

...

git rm -r --cached FILENAME_or_DIRECTORY

Crear un global ignore
++++++++++++++++++++++

git config --global core.excludesfile ~/.gitignore_global

Remover un brach local
++++++++++++++++++++++

git branch -d branch_name

Problem early EOF
+++++++++++++++++

git gc
git pull
