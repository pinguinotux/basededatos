Rclone
======

Rclone con google drive
+++++++++++++++++++++++

**rclone** permitirá tener un cliente de google drive desde una terminal Linux.
Se pueden hacer cosas similares a las que se haría en git pero desde drive.

Instalación:
------------

Entrar en consola copiar, pegar y ejecutar la siguiente instrucción:

.. code-block:: sh

    sudo apt install rclone

Configuración
-------------

A continuación se pondrá un ejemplo de configuración:

.. code-block:: sh

    rclone config 
    2017/09/29 17:05:35 NOTICE: Config file "/home/johnny/.config/rclone/rclone.conf" not found - using defaults
    No remotes found - make a new one
    n) New remote
    r) Rename remote
    c) Copy remote
    s) Set configuration password
    q) Quit config
    n/r/c/s/q> n
    name> remote
    Type of storage to configure.
    Choose a number from below, or type in your own value
     1 / Amazon Drive
       \ "amazon cloud drive"
     2 / Amazon S3 (also Dreamhost, Ceph, Minio)
       \ "s3"
     3 / Backblaze B2
       \ "b2"
     4 / Dropbox
       \ "dropbox"
     5 / Encrypt/Decrypt a remote
       \ "crypt"
     6 / Google Cloud Storage (this is not Google Drive)
       \ "google cloud storage"
     7 / Google Drive
       \ "drive"
     8 / Hubic
       \ "hubic"
     9 / Local Disk
       \ "local"
    10 / Microsoft OneDrive
       \ "onedrive"
    11 / Openstack Swift (Rackspace Cloud Files, Memset Memstore, OVH)
       \ "swift"
    12 / SSH/SFTP Connection
       \ "sftp"
    13 / Yandex Disk
       \ "yandex"
    Storage> 7
    Google Application Client Id - leave blank normally.
    client_id> 
    Google Application Client Secret - leave blank normally.
    client_secret> 
    Remote config
    Use auto config?
     * Say Y if not sure
     * Say N if you are working on a remote or headless machine or Y didn't work
    y) Yes
    n) No
    y/n> y
    If your browser doesn't open automatically go to the following link: http://127.0.0.1:53682/auth
    Log in and authorize rclone for access
    Waiting for code...
    Got code
    --------------------
    [remote]
    client_id = 
    client_secret = 
    token = {"access_token":" ","token_type":"Bearer","refresh_token":" ","expiry":"2017-09-29T18:08:33.50135122-05:00"}
    --------------------
    y) Yes this is OK
    e) Edit this remote
    d) Delete this remote
    y/e/d> y
    Current remotes:

    Name                 Type
    ====                 ====
    remote               drive

    e) Edit existing remote
    n) New remote
    d) Delete remote
    r) Rename remote
    c) Copy remote
    s) Set configuration password
    q) Quit config
    e/n/d/r/c/s/q> q

Comandos
--------

Clonar repositorio
******************

.. code-block:: sh

    rclone copy remote:path_directory ./

Donde path_directory se refiere al directorio que desea clonar de la cuenta google drive configurada.

Listar directorios remotos
**************************

.. code-block:: sh

    rclone lsd remote:

Actualizar
**********

Con la configuración ya hecha y estando en el directorio clonado puede tener su directorio 
local como sigue.

.. code-block:: sh

    rclone sync remote:path_directory ./

Donde path_directory se refiere al directorio que ha clonado de la cuenta google drive.

Subir un directorio
*******************

.. code-block:: sh

    rclone sync path_local remote:path_directory

Donde path_directory se refiere al directorio que ha clonado de la cuenta google drive configurada.
