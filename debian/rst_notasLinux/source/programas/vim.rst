VIM, más que un editor de texto
===============================

Atajos creados
++++++++++++++

.. code-block:: vim

        "Navegar por archivos abiertos buffer
        map <C-n> :bnext<CR>
        map <C-p> :bprevious<CR>


Soluciones
++++++++++

Colores en vim no se generan correctamente en lxterminal
--------------------------------------------------------

**Solución** Se debe a la configuración del archivo *.bashrc* 

Agregar las siguientes líneas en tal archivo:

.. code-block:: bash

	if [ "x$TERM" = "xxterm" ]
	then
    		export TERM="xterm-256color"
	fi
 

Buscar palabras
+++++++++++++++

Para encontrar una palabra entrar al modo comando y hacer como sigue:


.. code:: shell

    :/palabra_a_buscar

Para buscar más coincidencias, oprimir la tecla n en modo comandos.

Comandos
++++++++

1. Buscar y reemplazar todas las coincidencias.

   .. code-block:: vim
        
        :%s/word2find/replaceForthisWord/g


2. Ventanas:

En Vim podemos partir una ventana en dos tanto vertical como horizontalmente:

Ctrl + w v

Ctrl + w s

Para poder modificar su tamaño con respecto de las demás:

Añadir/Restar tamaño [n] vertical:

[n] Ctrl + w +

[n] Ctrl + w  –

Añadir/Restar tamaño [n] horizontal:

[n] Ctrl + w <
[n] Ctrl + w >

Establecer un tamaño igual para todas las ventanas (si es posible):

Ctrl + w =

Maximiza el tamaño de una ventana
Control+w _: Maximizar la ventana actual. 

3. Borrar Buffer

:bd ->  Permite cerrar la ventana abierta (quitar el archivo del buffer).

:buffers    ->  Muestra todos los buffer
:b [n]      -> Irá a un buffer.

Neobundle
+++++++++

Dependecias
-----------

.. code-block:: vim

        " Required:
        set runtimepath+=~/.vim/bundle/neobundle.vim/
        " Required:
        call neobundle#begin(expand('~/.vim/bundle/'))
        
        " Let NeoBundle manage NeoBundle
        " Required:
        NeoBundleFetch 'Shougo/neobundle.vim'

Airline
-------



`fuente <https://github.com/vim-airline/vim-airline>`_

.. code-block:: vim

        NeoBundle 'vim-airline/vim-airline'
        NeoBundle 'vim-airline/vim-airline-themes'

Configure:

.. code-block:: vim

        "Airline config
        set laststatus=2
        let g:airline_powerline_fonts = 1
        let g:airline_skip_empty_sections = 1


**Comandos**:
1. :AirlineTheme  

TagList
-------

.. code-block:: vim

        NeoBundle 'taglist.vim'


**Comandos**:
1. :Tlist

NerdTree
--------

.. code-block:: vim

        NeoBundle 'scrooloose/nerdtree'

**Comandos**:

1. :NERDTree
        
Tagbar
------

.. code-block:: vim

        " tagbar"
        NeoBundle 'majutsushi/tagbar'

**Comandos**
1. :Tagbar

Syntactis
---------

.. code-block:: vim

        NeoBundle 'vim-syntastic/syntastic'

 configuracion:

 .. code-block:: vim
	
	"Configure syntastic
	set statusline+=%#warningmsg#
	set statusline+=%{SyntasticStatuslineFlag()}
	set statusline+=%*
	let g:syntastic_always_populate_loc_list = 1
	let g:syntastic_auto_loc_list = 1
	let g:syntastic_check_on_open = 1
	let g:syntastic_check_on_wq = 0

 

Install package
---------------

.. code-block:: vim

        :NeoBundleInstall

Configuraciones VIMRC
---------------------

`https://github.com/hgfischer/vim/blob/master/vimrc <https://github.com/hgfischer/vim/blob/master/vimrc>`_

