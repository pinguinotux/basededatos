TMUX
====

Instalación de TMUX
+++++++++++++++++++

.. code-block:: sh

        apt-get install tmux

Problemas conocidos
+++++++++++++++++++

No carga el **.bashrc**:

Solución en el archivo **.profile** (crearlo o copiarlo) en la última
línea escribir lo siguiente:

.. code-block:: sh

        source ~/.bashrc
