.. _programas-index:

Programas Linux
======================

.. toctree::
    :maxdepth:  1
    :numbered:

    vim.rst
    wget.rst
    octave.rst
    sphinx.rst
    codeblocks.rst
    mutt.rst
    git.rst
    hipchat.rst
    latex.rst
    qucs.rst
    thunderbird.rst
    pidgin.rst
    gnuplot.rst
    diccionario.rst
    libreoffice.rst
    vlc.rst
    aptGet.rst
    notebook.rst
    tmux.rst
    rclone.rst
