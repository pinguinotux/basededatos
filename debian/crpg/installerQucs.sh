#!/bin/bash

# ANSI Escape Sequences
RED='\e[31;1m'
BLINK='\e[5;1m'
GREEN='\e[32;1m'
YELLOW='\e[33;1m'
TERM='\e[0m'
CLS='\e[2J'


# Log a message out to the console
function log {
	echo -e $1"$*"$TERM
}

here=`pwd`
carpeta=$here/qucs ; mkdir $carpeta ; cd $carpeta
log $BLINK "Vamos a instalar Qucs (Quite Universal Circuit Simulator)"
DLDIR=http://sourceforge.net/projects/qucs/files/qucs/0.0.18/qucs-0.0.18.tar.gz
DL=qucs-0.0.18.tar.gz
DIR=qucs-0.0.18
wget $DLDIR
tar -zxvf $DL
cd $DIR
#sudo apt-get install gawk clang 
sudo ./configure
log $GREEN "./configure done"
sudo make
log $GREEN "make done"
sudo make check
log $GREEN "make check done"
sudo make install
log $GREEN "make install done"
sudo make installcheck
log $GREEN "make installcheck done"

cd ../.. ; sudo rm -r $carpeta
