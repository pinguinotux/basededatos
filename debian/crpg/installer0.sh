#! /bin/bash

# ANSI Escape Sequences
RED='\e[31;1m'
BLINK='\e[5;1m'
GREEN='\e[32;1m'
YELLOW='\e[33;1m'
TERM='\e[0m'
CLS='\e[2J'

WARNING=${YELLOW}
ERROR=${RED}
NORMAL=${GREEN}
END=${TERM}

# Log a message out to the console
function log {
    echo -e $1"$*"$TERM
}

sudo apt-get update && sudo apt-get upgrade

sudo apt-get install vim git nmap minicom make gputils
log $GREEN "Instalados: vim, git, nmap, minicom, make, gputils"

sudo apt-get install scilab octave vlc tmux xournal oregano dia kicad verilog calcurse
log $GREEN "Instalados: scilab, octave, vlc, tmux, xournal, oregano, dia, kicad, verilog, calcurse"

sudo apt-get install texlive-full texmaker eclipse geany glade geda gparted icedove
log $GREEN "Instalados: texlive-full, texmaker, eclipse, geany, glade, geda, gparted, icedove"

sudo apt-get install logisim gsmc vokoscreen ngspice links youtube-dl mutt
log $GREEN "Instalados: logisim, gsmc, vokoscreen, ngspice, links, youtube-dl, mutt"

sudo apt-get install flashplugin-nonfree flex bison mplayer eog evince gpicview r-base-core r-cran-rcmdr
log $GREEN "Instalados: flashplugin-nonfree, flex, bison, mplayer, eog, evince, gpicview, r-base-core, r-cran-rcmdr"

sudo apt-get clean
log $BLINK "Finalizado"
