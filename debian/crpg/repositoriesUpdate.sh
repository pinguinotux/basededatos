#!/bin/bash

# ANSI Escape Sequences
RED='\e[31;1m'
BLINK='\e[5;1m'
GREEN='\e[32;1m'
YELLOW='\e[33;1m'
TERM='\e[0m'
CLS='\e[2J'

WARNING=${YELLOW}
ERROR=${RED}
NORMAL=${GREEN}
END=${TERM}

# Log a message out to the console
function log {
    echo -e $1"$*"$TERM
}


HOME=/home/carito


cd $HOME/basededatos ; log $GREEN "`pwd`"
git status ; git fetch origin master ; git pull origin master

cd $HOME/Bitbucket/carolinaPG/linux ; log $GREEN "`pwd`" 
git status ; git fetch origin master ; git pull origin master

cd $HOME/Bitbucket/carolinaPG/unal ; log $GREEN "`pwd`"
git status ; git fetch origin master ; git pull origin master

#cd $HOME/Bitbucket/carolinaPG/libros ; log $GREEN "`pwd`"
#git status ; git fetch origin master ; git pull origin master

cd $HOME/Bitbucket/carolinaPG/lineas-antenas ; log $GREEN "`pwd`"
git status ; git fetch origin master ; git pull origin master

cd $HOME/Bitbucket/carolinaPG/english-lessons ; log $GREEN "`pwd`"
git status ; git fetch origin master ; git pull origin master

cd $HOME/Bitbucket/carolinaPG/disenios_hardware ; log $GREEN "`pwd`"
git status ; git fetch origin master ; git pull origin master

cd $HOME/Bitbucket/carolinaPG/cursos ; log $GREEN "`pwd`"
git status ; git fetch origin master ; git pull origin master

cd $HOME/Bitbucket/johnnycubides/phoenix-control ; log $GREEN "`pwd`"
git status ; git fetch origin master ; git pull origin master

cd $HOME/Bitbucket/pinguinotux/tecnicasdeintegracion ; log $GREEN "`pwd`"
git status ; git fetch origin master ; git pull origin master

cd $HOME/Bitbucket/pinguinotux/pinguinolab ; log $GREEN "`pwd`"
git status ; git fetch origin master ; git pull origin master

cd $HOME/Bitbucket/pinguinotux/apuntes ; log $GREEN "`pwd`"
git status ; git fetch origin master ; git pull origin master

cd $HOME/Bitbucket/pinguinotux/luabot ; log $GREEN "`pwd`"
git status ; git fetch origin master ; git pull origin master

echo "TERMINADO"
