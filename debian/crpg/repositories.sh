#!/bin/bash

HOME=/home/carolina/

sudo rm -r $HOME/basededatos $HOME/Bitbucket  

cd $HOME
git clone https://carolinaPG@bitbucket.org/pinguinotux/basededatos.git

mkdir $HOME/Bitbucket
cd $HOME/Bitbucket
mkdir carolinaPG johnnycubides pinguinotux

cd $HOME/Bitbucket/carolinaPG
git clone https://carolinaPG@bitbucket.org/carolinaPG/linux.git
git clone https://carolinaPG@bitbucket.org/carolinaPG/unal.git
git clone https://carolinaPG@bitbucket.org/carolinaPG/disenios_hardware.git
mkdir Coursera; cd Coursera;
git clone https://carolinaPG@bitbucket.org/carolinaPG/electronesenaccion.git
git clone https://carolinaPG@bitbucket.org/carolinaPG/mostransistors.git

cd $HOME/Bitbucket/johnnycubides
git clone https://carolinaPG@bitbucket.org/johnnycubides/phoenix-control.git

cd $HOME/Bitbucket/pinguinotux
git clone https://carolinaPG@bitbucket.org/pinguinotux/tecnicasdeintegracion.git
git clone https://carolinaPG@bitbucket.org/pinguinotux/pinguinolab.git
git clone https://carolinaPG@bitbucket.org/pinguinotux/apuntes.git
git clone https://carolinaPG@bitbucket.org/pinguinotux/luabot.git

git config --global user.name "carolinaPG"
git config --global user.email crpulidog@unal.edu.co
git config --global core.editor vim
git config --list

echo "TERMINADO"
