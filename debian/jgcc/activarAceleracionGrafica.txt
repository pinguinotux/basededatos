Se puede comprobar aceleracion grafica a partir del siguiente paquete:
	mesa-utils
		comandos:
			glxinfo | grep direct
				retorna yes si hay aceleracion grafica (controlador de aceleracion)
			glxgears
				Ejecuta un programa donde se puede ver si se relantiza o no el repaint

Ejemplo de activacion grafica tarjetas graficas INTEL.
	Para hacer uso de este acelerador grafico se usa la aceleracion SNA que es propia 
	de las tarjetas graficas interl

	se crea un archivo xorg.conf el cual debe quedar en la direccion: /etc/X11/xorg.conf
	el contenido del archivo debe ser el siguiente:

Section "Device"
		Identifier "intel"
		Driver "intel"
		Option "AccelMethod" "sna"
		EndSection
	

Reiniciar el sistema y listo.
