#Herramientas Instaladas#

* designer
* pyuic5
* pyrcc5

## Uso de pyuic5 ## 

```sh
pyuic5 -x archivo_in.ui -o arhivo_out.py
```

pyuic5 permite convertir archivos **.ui** en archivos **.py**.

Los archivos **.ui** básicamente están escritos en *xml* y describen la vista de
los formularios o de las vistas logradas con Qt.

## Uso de pyrcc5 ##

Es una herramienta que permite emebeber recursos como lo son imagenes para
usarlos con pyqt

```sh
pyrcc5 -o archivo_out archivo_in.qrc
```

## Nota ##

Puede crear un script que permita hacer estos comandos de manera automática.
Ejemplo:
```sh
#!/bin/bash

pyuic5 -x ui/login.ui -o view/login.py
pyrcc5 -o archivo_out archivo_in.qrc
```

xml: eXtensible Markup Language en español: "Lenguaje de Marcas Extensible" Se trata de un metalenguaje (un lenguaje que se utiliza para decir algo acerca de otro) extensible de etiquetas.

Puede ver el siguiente link sobre más opciones.
[sobre pyqt](http://www.nexusmovil.com/curso-python-volumen-xiv-interfaces-graficas-parte-iii/) 
