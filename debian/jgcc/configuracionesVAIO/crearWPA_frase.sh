#!/bin/bash
#Este script crea un archivo de configuracion para
#ser usado por wpa_supplicant.

read -p "Nombre punto de acceso: " -r essid
read -p "Clave wpa: " -r clave

wpa_passphrase $essid $clave > ~/cuentasWPA/${essid}.conf

echo "Frase de clave wpa terminada para $essid" 
