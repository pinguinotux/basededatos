\select@language {spanish}
\contentsline {section}{\numberline {1}Programas}{1}{section.1}
\contentsline {subsection}{\numberline {1.1}Programas de reparacion}{1}{subsection.1.1}
\contentsline {subsubsection}{\numberline {1.1.1}grub rescue}{1}{subsubsection.1.1.1}
\contentsline {subsubsection}{\numberline {1.1.2}e2fsck}{1}{subsubsection.1.1.2}
\contentsline {subsubsection}{\numberline {1.1.3}fsck}{1}{subsubsection.1.1.3}
\contentsline {subsection}{\numberline {1.2}Gesti\IeC {\'o}n de Archivos}{1}{subsection.1.2}
\contentsline {subsubsection}{\numberline {1.2.1}alien}{1}{subsubsection.1.2.1}
\contentsline {subsection}{\numberline {1.3}Gestion de Repositorios}{1}{subsection.1.3}
\contentsline {subsubsection}{\numberline {1.3.1}Git}{1}{subsubsection.1.3.1}
\contentsline {paragraph}{Instalaci\IeC {\'o}n}{1}{section*.2}
\contentsline {paragraph}{Clonaci\IeC {\'o}n de repositorio}{2}{section*.3}
\contentsline {paragraph}{Modificando la \IeC {\'u}ltima confirmaci\IeC {\'o}n}{2}{section*.4}
\contentsline {paragraph}{Deshacer la preparaci\IeC {\'o}n de un Archivo}{2}{section*.5}
\contentsline {paragraph}{Deshacer Modificaci\IeC {\'o}n de un Archivo}{2}{section*.6}
\contentsline {paragraph}{Deshacer Modificaci\IeC {\'o}n de todos los archivos}{2}{section*.7}
\contentsline {section}{\numberline {2}Problemas}{2}{section.2}
\contentsline {subsection}{\numberline {2.1}Reparar sistema de ficheros}{2}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Tarjeta Grafica}{2}{subsection.2.2}
\contentsline {subsubsection}{\numberline {2.2.1}Aceleracion Tarjeta Grafica Intel}{2}{subsubsection.2.2.1}
\contentsline {section}{\numberline {3}Ajustes}{3}{section.3}
\contentsline {subsection}{\numberline {3.1}Quitar Beep del Sistema}{3}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Montar dispositivos usb}{3}{subsection.3.2}
