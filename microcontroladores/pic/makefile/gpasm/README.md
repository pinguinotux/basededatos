# PROYECTOS CON GPUTIL #

## GPASM ##

**Gpasm** es una herramienta del conjunto *gputils* con la cual
se puede ensamblar proyectos para microcontroladores de microchip

## GPDASM ##

**Gpdasm** puede obtener el archivo.asm a partir de un archivo.hex

## Descripcion del Makefile ##


El valor de doc debe ser sustituido por por el nombre del proyecto,
el dispositivo a usar es el microcontrolador a programar.
```
doc = proyecto
device = p16f876a
```

*assemble* permite crear el archivo.hex con el nivel de advertencias (w)
2, el formato de salida del archivo (-a) en inhx8m.
```
assembler: $(doc).asm
	gpasm -w 2 -a inhx8m $(doc).asm
```

*disassembler* permite que después de extraído el programa de la memoria
de un microcontrolador (la cual está en lenguaje máquina (opcode)) sea
desensamblado para su lectura en formato de instrucciones conocidas por
el programador. La salida del programa *gpdasm* es guardada en un archivo.
el argumento -i permite obtener una pequeña información acerca del 
archivo.hex, se podría hacer uso del argumento -s para que solo muestre 
el código, el argumento -p hace referencia al pic que fue leído.
```
disassembler:
	echo "`gpdasm -i -p $(device) $(doc)_well_read.hex`"\
			 > $(doc)_well_read.asm
```


Muestra la información listada en el archivo.lst que muestra también
los errores cometidos en el ensamblaje y cantidad de memori, entre otras.
```
error_view: 
	less $(doc).lst
```


Permite editar con vim nuestro proyecto
```
edit-a: 
	vim $(doc).asm
```


Al hacer uso de un *pickit* podemos ver con éste comando si el programador
está conectado a nuestro ordenador.
```
program-here:
	pk2cmd -?v
```


*program* programa y verifica el microcontrolador usado en el proyecto.
argumentos:
* -p -> Verifica y encuentra el pic a programar.
* -y -> Verifica si la programación fue satisfactoria.
* -f -> Tiene la dirección del programa hacer almacenado en la memoria.
```
program: $(doc).hex 
	pk2cmd -p -m -y -f$(doc).hex
```


*pic-read* lee el programa contenido en un microcontrolador y lo escribe
en un archivo en el directorio señalado.
```
pic-read: 
	pk2cmd -p -r -gf$(doc)_well_read.hex
```


*asm-view* Muestra el programa escrito por usted, mas no dejará que lo
modifique.
```
asm-view:
	less $(doc).asm
```


*clean* reinicia sus tareas.
```
clean: 
	rm *.hex *.cod *.lst
```


*clear* Limpia su directorio después de terminar su proyecto donde 
solo le interesa tener a mano los archivos más comunes para su 
programación
```
clear:
	rm *.lst
```
